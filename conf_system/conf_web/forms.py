from django import forms
from django.forms import fields, PasswordInput
from datetime import datetime, timedelta

from .models import Conference, Participant, ScientificArticle, TimeSlot, ArticleSession


class ConferenceForm(forms.ModelForm):
    participants = forms.ModelMultipleChoiceField(queryset=Participant.objects.all(), required=False, widget=forms.CheckboxSelectMultiple)
    class Meta:
        model = Conference
        fields = ['title', 'start_date', 'end_date', 'location', 'editors', 'program', 'topic', 'description',
                  'submission_rules', 'participants', 'base_template', 'session_length']
        widgets = {
            'start_date': forms.DateInput(attrs={'type': 'date'}),
            'end_date': forms.DateInput(attrs={'type': 'date'}),
            'editors': forms.CheckboxSelectMultiple,
            'participants': forms.CheckboxSelectMultiple,

        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Добавляем queryset для поля authors, чтобы отобразить всех участников
        self.fields['editors'].queryset = Participant.objects.all()
        self.fields['participants'].queryset = Participant.objects.all()


class ArticleForm(forms.ModelForm):
    time_slot = forms.ModelChoiceField(queryset=TimeSlot.objects.none(), required=False, label='Временной слот')

    class Meta:
        model = ScientificArticle
        fields = ['authors', 'title', 'abstract', 'text', 'article_file', 'time_slot']
        widgets = {
            'authors': forms.SelectMultiple,  # Изменение на выпадающий список
        }

    def __init__(self, *args, **kwargs):
        conference_id = kwargs.pop('conference_id', None)
        edit_mode = kwargs.pop('edit_mode', False)
        super(ArticleForm, self).__init__(*args, **kwargs)
        if conference_id:
            self.fields['time_slot'].queryset = TimeSlot.objects.filter(conference_id=conference_id, article_sessions__isnull=True)
        self.fields['authors'].queryset = Participant.objects.all()


class CommentForm(forms.ModelForm):
    class Meta:
        model = ScientificArticle
        fields = ['status', 'error_comments']  # предполагается, что у вас есть поле review_comments
        widgets = {
            'status': forms.Select(choices=ScientificArticle.ARTICLE_STATUS_CHOICES),
            'error_comments': forms.Textarea(attrs={'rows': 3, 'cols': 40})
        }

class BootstrapFormMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for field in self.fields.values():
        #     field.widget.attrs["class"] = field.widget.attrs.get("class", "")
        #     field.widget.attrs["class"] += " form-control"


class RegistrationForm(BootstrapFormMixin, forms.Form):
    username = fields.CharField(label="username")
    password = fields.CharField(label="Пароль", widget=PasswordInput())
    password2 = fields.CharField(label="Повторите пароль", widget=PasswordInput())
    full_name = fields.CharField(label="full_name")
    affiliation = fields.CharField(label="affiliation")

    def clean(self):
        cleaned_data = super(RegistrationForm, self).clean()
        if cleaned_data["password"] != cleaned_data["password2"]:
            self.add_error(None, "Пароли не совпадают")
        return cleaned_data


class LoginForm(BootstrapFormMixin, forms.Form):
    username = fields.CharField(label="username")
    password = fields.CharField(label="Пароль", widget=PasswordInput())

class TimeSlotForm(forms.ModelForm):
    class Meta:
        model = TimeSlot
        fields = ['conference', 'start_time', 'end_time']

class ArticleSessionForm(forms.ModelForm):
    class Meta:
        model = ArticleSession
        fields = ['article', 'time_slot', 'is_confirmed']
class ScheduleGenerationForm(forms.Form):
    slot_duration = forms.DurationField(label='Длительность слота', help_text='Например, 00:30:00 для 30 минут')
    break_duration = forms.DurationField(label='Длительность перерыва', help_text='Например, 00:10:00 для 10 минут')
    slots_per_break = forms.IntegerField(label='Количество слотов до перерыва', min_value=1)
    work_hours_start = forms.TimeField(label='Начало рабочего дня', help_text='Например, 09:00')
    work_hours_end = forms.TimeField(label='Конец рабочего дня', help_text='Например, 18:00')
    days = forms.MultipleChoiceField(
        label='Выберите дни',
        widget=forms.CheckboxSelectMultiple,
        choices=[]
    )

    def __init__(self, *args, **kwargs):
        conference_id = kwargs.pop('conference_id', None)
        super(ScheduleGenerationForm, self).__init__(*args, **kwargs)
        if conference_id:
            conference = Conference.objects.get(pk=conference_id)
            days = [(conference.start_date.date() + timedelta(days=i), (conference.start_date.date() + timedelta(days=i)).strftime('%Y-%m-%d')) for i in range((conference.end_date.date() - conference.start_date.date()).days + 1)]
            self.fields['days'].choices = days