# Generated by Django 5.0.4 on 2024-04-14 17:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('conf_web', '0004_remove_scientificarticle_status2'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='scientificarticle',
            options={'verbose_name': 'Научная статья', 'verbose_name_plural': 'Научные статьи'},
        ),
        migrations.AddField(
            model_name='scientificarticle',
            name='article_file',
            field=models.FileField(blank=True, null=True, upload_to='articles/%Y/%m/%d/'),
        ),
    ]
