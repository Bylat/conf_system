# Generated by Django 5.0.4 on 2024-04-20 17:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('conf_web', '0011_alter_participant_options_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='participant',
            name='articles',
            field=models.ManyToManyField(related_name='articles', to='conf_web.scientificarticle'),
        ),
        migrations.AddField(
            model_name='participant',
            name='conferences',
            field=models.ManyToManyField(related_name='conferences', to='conf_web.conference'),
        ),
        migrations.AlterField(
            model_name='scientificarticle',
            name='status',
            field=models.CharField(choices=[('ожидает рецензирования', 'Ожидает рецензирования'), ('принята', 'Принята'), ('отклонена', 'Отклонена')], max_length=30),
        ),
    ]
