# Generated by Django 5.0.4 on 2024-04-14 15:02

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('conf_web', '0002_delete_conference'),
    ]

    operations = [
        migrations.CreateModel(
            name='Conference',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('start_date', models.DateTimeField()),
                ('end_date', models.DateTimeField()),
                ('location', models.CharField(max_length=255)),
                ('program', models.TextField()),
                ('topic', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('submission_rules', models.TextField()),
                ('status', models.CharField(choices=[('активная', 'Активная'), ('завершена', 'Завершена')], default='активная', max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Participant',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('email', models.EmailField(max_length=254, unique=True)),
                ('full_name', models.CharField(max_length=100)),
                ('affiliation', models.CharField(max_length=255)),
                ('role', models.CharField(choices=[('participant', 'Participant'), ('organizer', 'Organizer'), ('reviewer', 'Reviewer')], max_length=20)),
                ('is_active', models.BooleanField(default=True)),
                ('is_staff', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='ScientificArticle',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('abstract', models.TextField()),
                ('text', models.TextField()),
                ('status', models.CharField(choices=[('ожидает рецензирования', 'Ожидает рецензирования'), ('принята', 'Принята1'), ('отклонена', 'Отклонена')], max_length=30)),
                ('status2', models.CharField(choices=[('ожидает рецензирования', 'Ожидает рецензирования'), ('принята', 'Принята1'), ('отклонена', 'Отклонена')], max_length=30)),
                ('authors', models.ManyToManyField(to='conf_web.participant')),
                ('conference', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='conf_web.conference')),
            ],
        ),
    ]
