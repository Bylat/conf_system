# Generated by Django 5.0.4 on 2024-04-13 19:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('conf_web', '0001_initial'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Conference',
        ),
    ]
