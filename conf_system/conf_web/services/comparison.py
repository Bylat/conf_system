import re


def extract_all_elements(latex_file):
    with open(latex_file, 'r', encoding='utf-8') as file:
        latex_content = file.read()

    # Используем регулярное выражение для разбиения содержимого LaTeX на различные элементы
    elements = re.findall(r'\\[a-zA-Z]+(?:\[[^\]]*\])?(?:{[^}]*})?', latex_content)

    return elements


def check_template_elements(article_text, template_elements):
    matched_elements = []

    for element in template_elements:
        pattern = re.escape(element)
        if re.search(pattern, article_text):
            matched_elements.append(element)

    return matched_elements


def extract_format_info(latex_file):
    with open(latex_file, 'r', encoding='utf-8') as file:
        latex_content = file.read()

    # Поиск информации о классе документа
    documentclass_pattern = r'\\documentclass(?:\[[^\]]*\])?{([^}]*)}'
    document_class_match = re.search(documentclass_pattern, latex_content)
    document_class = document_class_match.group(1) if document_class_match else None

    # Поиск информации о подключенных пакетах
    usepackage_pattern = r'\\usepackage(?:\[[^\]]*\])?{([^}]*)}'
    used_packages = re.findall(usepackage_pattern, latex_content)

    # Поиск информации о других настройках (например, интервалы, размеры шрифтов)
    other_settings_pattern = r'\\(linespread|setlength|renewcommand)(?:\{[^}]*\}){1,2}'
    other_settings = re.findall(other_settings_pattern, latex_content)

    # Поиск информации о размере шрифта
    fontsize_pattern = r'\\(tiny|scriptsize|footnotesize|small|normalsize|large|Large|LARGE|huge|Huge)'
    fontsize_match = re.search(fontsize_pattern, latex_content)
    fontsize = fontsize_match.group(1) if fontsize_match else None

    return document_class, used_packages, other_settings, fontsize


def compare_format_info(article_file, template_file):
    # Извлечение информации о форматировании из статьи и шаблона
    article_info = extract_format_info(article_file)
    template_info = extract_format_info(template_file)

    # Сравнение информации
    matched_packages = [pkg for pkg in article_info[1] if pkg in template_info[1]]
    matched_settings = [setting for setting in article_info[2] if setting in template_info[2]]
    font_match = (article_info[3] == template_info[3])

    # Вычисление процента совпадения пакетов
    total_packages_count = len(template_info[1])
    matched_packages_count = len(matched_packages)
    percentage_matched_packages = (
                                              matched_packages_count / total_packages_count) * 100 if total_packages_count > 0 else 0

    # Вычисление процента совпадения по настройкам
    total_settings_count = len(template_info[2])
    matched_settings_count = len(matched_settings)
    percentage_matched_settings = (
                                              matched_settings_count / total_settings_count) * 100 if total_settings_count > 0 else 0

    # Вычисление процента совпадения по шрифту
    font_match_percentage = 100 if font_match else 0

    # Общий процент совпадения
    overall_percentage = (percentage_matched_packages + percentage_matched_settings + font_match_percentage) / 3
    return overall_percentage


def find_missing_elements(article_text, template_elements):
    missing_elements = []

    for element in template_elements:
        pattern = re.escape(element)
        if not re.search(pattern, article_text):
            missing_elements.append(element)

    return missing_elements


def check_article_against_template(article_file, template_file):
    # Собираем все элементы шаблона из файла LaTeX
    template_elements = extract_all_elements(template_file)

    # Читаем текст статьи из файла
    with open(article_file, 'r', encoding='utf-8') as f:
        article_text = f.read()

    # Вызываем функцию для сравнения информации о форматировании
    format_match_percentage = compare_format_info(article_file, template_file)

    # Проверяем совпадение элементов из шаблона с элементами статьи
    matched_elements = check_template_elements(article_text, template_elements)

    # Находим различающиеся элементы между статьей и шаблоном
    differing_elements = find_missing_elements(article_text, template_elements)

    # Проверка общего совпадения
    if len(template_elements) > 0:
        percentage_matched_elements = (len(matched_elements) / len(template_elements)) * 100
    else:
        percentage_matched_elements = 0.0  # если шаблон пустой

    # Подготовка результатов проверки
    if (percentage_matched_elements == 100 and format_match_percentage == 100) or \
            (percentage_matched_elements >= 50 and format_match_percentage >= 50):
        status = True  # Статья соответствует шаблону
    else:
        status = False  # Статья не соответствует шаблону

    # Формирование списка для комментариев об ошибках
    error_comments =  differing_elements  # Список различающихся элементов

    return status, error_comments

    # Пример использования
    article_file = '4.tex'
    template_file = 'template_4.tex'

    status, error_comments = check_article_against_template(article_file, template_file)

    if status:
        print("Статья соответствует шаблону.")
    else:
        print("Статья не соответствует шаблону.")
        if error_comments:
            print("Элементы шаблона, отсутствующие в статье:")
            for element in error_comments:
                print(element)
