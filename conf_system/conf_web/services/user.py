from conf_web.models import Participant


def register_user(username: str, password: str, full_name, affiliation):
    user = Participant(username=username, full_name=full_name, affiliation=affiliation)
    user.set_password(password)
    user.save()
    return user
