from django.urls import path, include
from conf_web import views

from .views import register_view, logout_view, edit_comment, generate_schedule, conference_schedule, \
    add_article_to_slot, generate_time_slots, distribute_schedule

urlpatterns = [
    path('home/', views.index, name='home'),
    path('conferences/', views.conference_list, name='conference_list'),
    path('conferences/<int:pk>/', views.conference_detail, name='conference_detail'),
    path('conferences/create/', views.conference_create, name='conference_create'),
    path('conferences/<int:pk>/edit/', views.conference_edit, name='conference_edit'),
    path('conferences/<int:pk>/delete/', views.conference_delete, name='conference_delete'),
    path('conferences/<int:conference_id>/create_time_slot/', views.create_time_slot, name='create_time_slot'),
    path('conferences/<int:conference_id>/generate_schedule/', views.generate_schedule, name='generate_schedule'),
    path('conferences/<int:conference_id>/schedule/', conference_schedule, name='conference_schedule'),
    path('conferences/<int:conference_id>/add_article_to_slot/', add_article_to_slot, name='add_article_to_slot'),
    path('conferences/<int:conference_id>/generate_time_slots/', generate_time_slots, name='generate_time_slots'),
    path('conferences/<int:conference_id>/distribute_schedule/', distribute_schedule, name='distribute_schedule'),

    path('articles/', views.article_list, name='article_list'),
    path('articles/<int:pk>/', views.article_detail, name='article_detail'),
    path('articles/<int:article_id>/edit_comment/', edit_comment, name='edit-comment'),
    path('articles/create/<int:conference_id>/', views.article_create, name='article_create'),
    path('articles/<int:pk>/edit/', views.article_edit, name='article_edit'),
    path('articles/<int:pk>/delete/', views.article_delete, name='article_delete'),
    path('conferences/<int:pk>/articles/', views.conference_articles, name='conference_articles'),
    path('profile/', views.user_profile, name='user_profile'),
    path('register/', register_view, name='register'),
    path('login/', views.login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('join/<int:conference_id>/', views.join_conference, name='join_conference'),
    path('scheduler/', include('schedule.urls')),  # Убедитесь, что у вас есть этот путь для интеграции django-scheduler
]
