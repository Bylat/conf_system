import pytz
from datetime import timedelta, datetime
from functools import wraps


from django.contrib.auth import login, authenticate, logout
from django.db import transaction
from django.http import HttpResponseForbidden, HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect

from .forms import ConferenceForm, RegistrationForm, LoginForm, ArticleForm, CommentForm, TimeSlotForm, \
    ArticleSessionForm, ScheduleGenerationForm
from .models import Conference, Participant, ScientificArticle, TimeSlot, ArticleSession
from .services.user import register_user
from .services.comparison import check_article_against_template
from schedule.models import Calendar, Event


def register_view(request):
    context = {"form": RegistrationForm()}
    if request.method == "POST":
        form = RegistrationForm(request.POST, request.FILES)
        context["form"] = form
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            full_name = form.cleaned_data["full_name"]
            affiliation = form.cleaned_data["affiliation"]
            register_user(username, password, full_name, affiliation)
            context["message"] = "Регистрация прошла успешно"
            return redirect("login")
    return render(request, "web/register.html", context)


@csrf_protect
def login_view(request):
    form = LoginForm()
    context = {'form': form}
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is None:
                form.add_error(None, "Email или пароль неверные")
                context["form"] = form
            else:
                login(request, user)
                next_url = request.POST.get("conferences")
                if next_url != "":
                    return redirect("home")
                return redirect("home")
    return render(request, "web/login.html", context)


def logout_view(request):
    logout(request)
    return redirect("home")


def author_required(view_func):
    @wraps(view_func)
    def _wrapped_view(request, *args, **kwargs):
        pk = kwargs.get('pk')
        article = get_object_or_404(ScientificArticle, pk=pk)
        if request.user not in article.authors.all():
            # Если текущий пользователь не является автором статьи, возвращаем ошибку доступа
            return HttpResponseForbidden("You don't have permission to edit this article.")
        return view_func(request, *args, **kwargs)

    return _wrapped_view


# Список всех конференций
def conference_list(request):
    conferences = Conference.objects.all()
    return render(request, 'conference/conference_list.html', {'conferences': conferences, 'user': request.user})


# Детали конкретной конференции
def conference_detail(request, pk):
    conference = get_object_or_404(Conference, pk=pk)
    articles = conference.scientificarticle_set.all()  # Получение списка статей для конференции

    return render(request, 'conference/conference_detail.html',
                  {'conference': conference, 'user': request.user, 'articles': articles})


# Создание конференции (только для организаторов)
@login_required
def conference_create(request):
    if request.method == 'POST':
        form = ConferenceForm(request.POST, request.FILES)
        if form.is_valid():
            conference = form.save(commit=False)
            conference.save()  # Сохраняем конференцию в базе данных

            # Добавляем редакторов конференции
            editors_ids = request.POST.getlist('editors')
            editors = Participant.objects.filter(id__in=editors_ids)
            for editor in editors:
                conference.editors.add(editor)
            conference.save()
            # Добавляем участников конференции
            participants_ids = request.POST.getlist('participants')
            participants = Participant.objects.filter(id__in=participants_ids)
            for participant in participants:
                conference.editors.add(participant)
            conference.save()
            # Перенаправляем на страницу с деталями только что созданной конференции
            return redirect('conference_detail', pk=conference.pk)
    else:
        form = ConferenceForm()
    # Получаем список всех участников для выбора редакторов
    participants = Participant.objects.all()
    return render(request, 'conference/conference_create.html', {'form': form, 'participants': participants})


# Редактирование конференции (только для организаторов)
@login_required
def conference_edit(request, pk):
    conference = get_object_or_404(Conference, pk=pk)

    # Проверяем, является ли текущий пользователь редактором конференции
    if request.user not in conference.editors.all():
        return HttpResponseForbidden("You don't have permission to edit this conference.")

    if request.method == 'POST':
        form = ConferenceForm(request.POST, request.FILES, instance=conference)
        if form.is_valid():
            form.save()
            return redirect('conference_detail', pk=pk)
    else:
        form = ConferenceForm(instance=conference)

    return render(request, 'conference/conference_edit.html', {'form': form, 'conference': conference})


# Удаление конференции (только для организаторов)
@login_required
def conference_delete(request, pk):
    conference = get_object_or_404(Conference, pk=pk)

    # Проверяем, является ли текущий пользователь редактором конференции
    if request.user not in conference.editors.all():
        return HttpResponseForbidden("You don't have permission to delete this conference.")

    if request.method == 'POST':
        conference.delete()
        return redirect('conference_list')

    return render(request, 'conference/conference_delete.html', {'conference': conference})


# Список всех научных статей
def article_list(request):
    articles = ScientificArticle.objects.all()
    return render(request, 'article/article_list.html', {'articles': articles, 'user': request.user})


# Детали конкретной научной статьи
def article_detail(request, pk):
    article = get_object_or_404(ScientificArticle, pk=pk)
    is_editor = request.user in article.conference.editors.all()
    # Открываем файл статьи
    preferred_time_slots = article.preferred_time_slots.all()

    file_path = article.article_file.path
    with open(file_path, 'rb') as file:
        file_content = file.read()
    response = HttpResponse(file_content, content_type='application/octet-stream')
    response[
        'Content-Disposition'] = 'inline; filename="' + article.article_file.name + '"'  # Имя файла в заголовке Content-Disposition

    return render(request, 'article/article_detail.html',
                  {'article': article, 'user': request.user, 'is_editor': is_editor})


# Создание научной статьи (доступно только участникам)
@login_required
def article_create(request, conference_id):
    conference = get_object_or_404(Conference, pk=conference_id)
    if request.method == 'POST':
        form = ArticleForm(request.POST, request.FILES, conference_id=conference_id)
        if form.is_valid():
            article = form.save(commit=False)
            article.conference_id = conference_id
            article.save()
            authors = form.cleaned_data['authors']
            article.authors.add(*authors)
            article.save()

            base_template_path = conference.base_template.path
            article_path = article.article_file.path
            status, error_comments = check_article_against_template(article_path, base_template_path)
            time_slot = form.cleaned_data['time_slot']
            if time_slot:
                ArticleSession.objects.create(article=article, time_slot=time_slot, is_confirmed=True)
            if status:
                article.status = 'ожидает рецензирования'
            else:
                article.status = 'отклонена'
                error_comments_str = "\n".join(error_comments)
                article.error_comments = error_comments_str

            article.save()
            return redirect('conference_detail', pk=article.conference.pk)
    else:
        form = ArticleForm(conference_id=conference_id)
    return render(request, 'article/article_create.html', {'form': form, 'conference': conference})


# Редактирование научной статьи (доступно только авторам)
@login_required
def article_edit(request, pk):
    article = get_object_or_404(ScientificArticle, pk=pk)
    conference_id = article.conference_id
    # Проверяем, является ли текущий пользователь автором статьи
    if request.user not in article.authors.all():
        return HttpResponseForbidden("You don't have permission to edit this article.")

    if request.method == 'POST':
        form = ArticleForm(request.POST, request.FILES, instance=article, conference_id=conference_id)
        if form.is_valid():
            article = form.save(commit=False)
            form.save()

            base_template_path = article.conference.base_template.path
            article_path = article.article_file.path
            status, error_comments = check_article_against_template(article_path, base_template_path)
            time_slot = form.cleaned_data['time_slot']
            if time_slot:
                article.preferred_time_slots.clear()  # Удаление всех текущих временных слотов
                article.preferred_time_slots.add(time_slot)

            if status:
                article.status = 'ожидает рецензирования'
                article.error_comments = ''
            else:
                article.status = 'отклонена'
                error_comments_str = "\n".join(error_comments)
                article.error_comments = "Список различающихся элементов между статьей и шаблоном\n" + error_comments_str

            article.save()
            return redirect('article_detail', pk=article.pk)
    else:
        form = ArticleForm(instance=article, conference_id=conference_id)
    return render(request, 'article/article_edit.html', {'form': form, 'article': article})


# Удаление научной статьи (доступно только авторам)
@login_required
def article_delete(request, pk):
    article = get_object_or_404(ScientificArticle, pk=pk)
    if request.user not in article.authors.all():
        # Если текущий пользователь не является автором статьи, возвращаем ошибку доступа
        return HttpResponseForbidden("You don't have permission to delete this article.")

    if request.method == 'POST':
        # Подтверждение удаления статьи
        article.delete()
        return redirect('article_list')

    return render(request, 'article/article_delete.html', {'article': article})


def conference_articles(request, pk):
    conference = get_object_or_404(Conference, pk=pk)
    articles = conference.scientificarticle_set.all()
    return render(request, 'conf_web/conference_articles.html',
                  {'conference': conference, 'articles': articles, 'user': request.user})


@login_required
def edit_comment(request, article_id):
    article = get_object_or_404(ScientificArticle, id=article_id)
    if request.user not in article.conference.editors.all():
        return HttpResponseForbidden("Вы не имеете права рецензировать эту статью.")

    if request.method == 'POST':
        form = CommentForm(request.POST, instance=article)
        if form.is_valid():
            form.save()
            return redirect('article_detail', pk=article.pk)
    else:
        form = CommentForm(instance=article)

    return render(request, 'article/edit_comment.html', {'form': form, 'article': article})


# Личная страница пользователя
@login_required
def user_profile(request):
    user = request.user
    conferences = Conference.objects.filter(participants=user)
    my_articles = ScientificArticle.objects.filter(authors=user)
    #   if user.role == 'reviewer' or user.role == 'Reviewer':
    # Получаем конференции, в которых пользователь является редактором
    editor_conferences = Conference.objects.filter(editors=request.user)
    # Получаем статьи на проверку для каждой конференции
    review_articles = []
    for conference in editor_conferences:
        articles = ScientificArticle.objects.filter(conference=conference, status='ожидает рецензирования')
        review_articles.extend(articles)

    return render(request, 'web/user_profile.html',
                  {'user': user, 'conferences': editor_conferences, 'articles': my_articles,
                   'review_articles': review_articles})


@login_required
def join_conference(request, conference_id):
    conference = get_object_or_404(Conference, pk=conference_id)

    # Проверяем, является ли пользователь участником этой конференции
    if conference.participants.filter(id=request.user.id).exists():
        message = 'Вы уже участник этой конференции.'
    else:
        # Добавляем пользователя в список участников конференции
        conference.participants.add(request.user)
        message = 'Вы успешно присоединились к конференции.'

    return render(request, 'conference/join_conference.html', {'conference': conference, 'message': message})


def index(request):
    return render(request, "web/main.html")


@login_required
def schedule_article(request, article_id):
    article = get_object_or_404(ScientificArticle, pk=article_id)
    if request.method == 'POST':
        form = ArticleSessionForm(request.POST)
        if form.is_valid():
            article_session = form.save(commit=False)
            article_session.article = article
            article_session.save()
            return redirect('article_detail', pk=article.pk)
    else:
        form = ArticleSessionForm()
    return render(request, 'article/article_session_form.html', {'form': form})


@login_required
def create_time_slot(request, conference_id):
    conference = get_object_or_404(Conference, pk=conference_id)
    if request.method == 'POST':
        form = TimeSlotForm(request.POST)
        if form.is_valid():
            time_slot = form.save(commit=False)
            time_slot.conference = conference
            time_slot.save()
            return redirect('conference_schedule', conference_id=conference.pk)
    else:
        form = TimeSlotForm(initial={'conference': conference})
    return render(request, 'schedule/add_time_slot.html', {'form': form, 'conference': conference})

@login_required
def add_article_to_slot(request, conference_id):
    conference = get_object_or_404(Conference, pk=conference_id)
    if request.method == 'POST':
        form = ArticleSessionForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('conference_schedule', conference_id=conference.pk)
    else:
        form = ArticleSessionForm()
        form.fields['article'].queryset = ScientificArticle.objects.filter(conference=conference)
        form.fields['time_slot'].queryset = TimeSlot.objects.filter(conference=conference)
    return render(request, 'schedule/add_article_to_slot.html', {'form': form, 'conference': conference})

@login_required
def generate_schedule(request, conference_id):
    conference = get_object_or_404(Conference, pk=conference_id)
    calendar, created = Calendar.objects.get_or_create(name=conference.title)
    articles = ScientificArticle.objects.filter(conference=conference, status='принята')

    start_time = conference.start_date
    for article in articles:
        duration = article.duration
        end_time = start_time + duration

        # Создаем новый временной слот для статьи
        time_slot = TimeSlot.objects.create(conference=conference, start_time=start_time, end_time=end_time)

        # Создаем сессию для статьи
        ArticleSession.objects.create(article=article, time_slot=time_slot, is_confirmed=True)

        # Создаем событие в календаре
        Event.objects.create(
            title=article.title,
            start=start_time,
            end=end_time,
            calendar=calendar,
            description=article.abstract
        )

        # Обновляем время начала для следующей статьи
        start_time = end_time + timedelta(minutes=10)  # Добавляем перерыв между сессиями

    return redirect('conference_detail', pk=conference.pk)


@login_required
def create_conference_schedule(request, conference_id):
    conference = get_object_or_404(Conference, pk=conference_id)
    if request.method == 'POST':
        form = ConferenceForm(request.POST, instance=conference)
        if form.is_valid():
            form.save()
            return redirect('conference_detail', pk=conference.pk)
    else:
        form = ConferenceForm(instance=conference)
    return render(request, 'conference/conference_form.html', {'form': form})


def assign_articles_to_time_slots(conference):
    # Получаем все статьи для конференции
    articles = ScientificArticle.objects.filter(conference=conference, status='принята')
    time_slots = conference.time_slots.all()

    for article, time_slot in zip(articles, time_slots):
        ArticleSession.objects.create(
            article=article,
            time_slot=time_slot,
            is_confirmed=True
        )


@login_required
def conference_schedule(request, conference_id):
    conference = get_object_or_404(Conference, pk=conference_id)
    time_slots = TimeSlot.objects.filter(conference=conference)

    slot_list = []
    for slot in time_slots:
        article_session = ArticleSession.objects.filter(time_slot=slot).first()
        if article_session:
            slot_list.append({
                'title': article_session.article.title,
                'start': slot.start_time.isoformat(),
                'end': slot.end_time.isoformat(),
                'color': 'red'  # Color for occupied slots
            })
        else:
            slot_list.append({
                'title': 'Свободно',
                'start': slot.start_time.isoformat(),
                'end': slot.end_time.isoformat(),
                'color': 'green'  # Color for free slots
            })

    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        return JsonResponse(slot_list, safe=False)

    return render(request, 'conference/conference_schedule.html', {'conference': conference})
@login_required
def generate_time_slots(request, conference_id):
    conference = get_object_or_404(Conference, pk=conference_id)
    msk_timezone = pytz.timezone('Europe/Moscow')

    if request.method == 'POST':
        form = ScheduleGenerationForm(request.POST, conference_id=conference_id)
        if form.is_valid():
            slot_duration = form.cleaned_data['slot_duration']
            break_duration = form.cleaned_data['break_duration']
            slots_per_break = form.cleaned_data['slots_per_break']
            work_hours_start = form.cleaned_data['work_hours_start']
            work_hours_end = form.cleaned_data['work_hours_end']
            selected_days = form.cleaned_data['days']

            with transaction.atomic():
                for day in selected_days:
                    day_start = datetime.combine(datetime.strptime(day, '%Y-%m-%d').date(), datetime.min.time())
                    day_end = datetime.combine(datetime.strptime(day, '%Y-%m-%d').date(), datetime.max.time())

                    # Удаляем старые временные слоты для выбранного дня
                    TimeSlot.objects.filter(conference=conference, start_time__gte=day_start, end_time__lte=day_end).delete()

                    # Генерируем новые временные слоты
                    current_time = msk_timezone.localize(datetime.combine(datetime.strptime(day, '%Y-%m-%d').date(), work_hours_start))
                    end_time = msk_timezone.localize(datetime.combine(datetime.strptime(day, '%Y-%m-%d').date(), work_hours_end))
                    slot_count = 0

                    while current_time + slot_duration <= end_time:
                        TimeSlot.objects.create(
                            conference=conference,
                            start_time=current_time,
                            end_time=current_time + slot_duration
                        )
                        current_time += slot_duration
                        slot_count += 1

                        if slot_count % slots_per_break == 0:
                            current_time += break_duration

            return redirect('conference_schedule', conference_id=conference.pk)
    else:
        form = ScheduleGenerationForm(conference_id=conference_id)

    return render(request, 'schedule/generate_time_slots.html', {'form': form, 'conference': conference})
@login_required
def distribute_schedule(request, conference_id):
    conference = get_object_or_404(Conference, pk=conference_id)
    articles = ScientificArticle.objects.filter(conference=conference, status='ожидает рецензирования')

    conflict_articles = []

    for article in articles:
        preferred_slots = article.preferred_time_slots.all()
        assigned = False

        for slot in preferred_slots:
            if not ArticleSession.objects.filter(time_slot=slot).exists():
                ArticleSession.objects.create(article=article, time_slot=slot, is_confirmed=True)
                assigned = True
                break

        if not assigned:
            conflict_articles.append(article)

    return render(request, 'schedule/distribute_schedule.html', {'conference': conference, 'conflict_articles': conflict_articles})
