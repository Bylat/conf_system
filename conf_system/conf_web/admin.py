from django.contrib import admin
from .models import Conference, ScientificArticle, Participant, TimeSlot, ArticleSession
from django.contrib import admin


@admin.register(Conference)
class ConferenceAdmin(admin.ModelAdmin):
    list_display = ['title', 'status', 'start_date', 'end_date']


@admin.register(TimeSlot)
class TimeSlotAdmin(admin.ModelAdmin):
    list_display = ['conference', 'start_time', 'end_time']


@admin.register(ArticleSession)
class ArticleSessionAdmin(admin.ModelAdmin):
    list_display = ['article', 'time_slot', 'is_confirmed']
