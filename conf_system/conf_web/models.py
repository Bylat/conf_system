from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
import uuid  # Для генерации уникальной ссылки
from django.utils import timezone
from schedule.models import Calendar, Event


class ParticipantManager(BaseUserManager):
    def create_user(self, username, password=None, **extra_fields):
        user = self.model(username=username)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, password=None, **extra_fields):
        user = self.model(username=username, is_staff=True, is_superuser=True)
        user.set_password(password)
        user.save()
        return user


# Модель участника
class Participant(AbstractBaseUser, PermissionsMixin):
    objects = ParticipantManager()
    username = models.CharField(max_length=150, unique=True)
    password = models.CharField(max_length=128, verbose_name="Пароль")
    full_name = models.CharField(max_length=100)
    affiliation = models.CharField(max_length=255)
    ROLE_CHOICES = (
        ('participant', 'Participant'),
        ('organizer', 'Organizer'),
        ('reviewer', 'Reviewer'),
    )
    role = models.CharField(max_length=20, choices=ROLE_CHOICES)
    articles = models.ManyToManyField('ScientificArticle', related_name='articles')
    conferences = models.ManyToManyField('Conference', related_name='conferences')

    USERNAME_FIELD = 'username'

    class Meta:
        app_label = 'conf_web'
        verbose_name = "пользователь"
        verbose_name_plural = "пользователи"

    def __str__(self):
        return self.username


# Модель конференции
class Conference(models.Model):
    objects = None
    title = models.CharField(max_length=100)  # Название конференции
    start_date = models.DateTimeField()  # Дата и время начала
    end_date = models.DateTimeField()  # Дата и время окончания
    location = models.CharField(max_length=255)  # Место проведения
    program = models.TextField()  # Программа конференции
    topic = models.CharField(max_length=255)  # Тематика и область конференции
    description = models.TextField()  # Описание конференции
    submission_rules = models.TextField()  # Правила подачи и форматы работы
    STATUS_CHOICES = (
        ('активная', 'Активная'),
        ('завершена', 'Завершена'),
    )
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='активная')  # Статус конференции
    editors = models.ManyToManyField(Participant)
    participants = models.ManyToManyField(Participant, related_name='participant_conferences')
    base_template = models.FileField(upload_to='templates/%Y/%m/%d/', null=True, blank=True)
    join_link = models.CharField(max_length=100, default=uuid.uuid4().hex[:8])  # Уникальная ссылка для присоединения
    session_length = models.DurationField(default=timezone.timedelta(minutes=30))
    calendar = models.OneToOneField(Calendar, on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        app_label = 'conf_web'

    def __str__(self):
        return self.title


class TimeSlot(models.Model):
    conference = models.ForeignKey(Conference, related_name='time_slots', on_delete=models.CASCADE)
    start_time = models.DateTimeField(default=timezone.now)
    end_time = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f"{self.start_time.strftime('%Y-%m-%d %H:%M')} - {self.end_time.strftime('%Y-%m-%d %H:%M')}"


# Модель научной статьи
class ScientificArticle(models.Model):
    objects = None
    conference = models.ForeignKey(Conference, on_delete=models.CASCADE)  # Один ко многим с конференциями
    authors = models.ManyToManyField(Participant)  # Многие ко многим с участниками
    participants = models.ManyToManyField(Participant, related_name='participant_articles')
    title = models.CharField(max_length=255)  # Название статьи
    abstract = models.TextField()  # Аннотация
    text = models.TextField()  # Текст статьи
    ARTICLE_STATUS_CHOICES = (
        ('ожидает рецензирования', 'Ожидает рецензирования'),
        ('принята', 'Принята'),
        ('отклонена', 'Отклонена'),
    )
    status = models.CharField(max_length=30, choices=ARTICLE_STATUS_CHOICES)  # Статус статьи
    # Поле для загрузки файла статьи
    article_file = models.FileField(upload_to='articles/%Y/%m/%d/', blank=True, null=True)
    error_comments = models.TextField(blank=True, null=True)  # Поле для комментариев к ошибкам

    # Новые поля
    preferred_time_slots = models.ManyToManyField(TimeSlot, blank=True,
                                                  help_text="Preferred time slots for presentation.")
    duration = models.DurationField(default=timezone.timedelta(minutes=30), help_text="Duration of the presentation.")
    special_requirements = models.TextField(blank=True, null=True,
                                            help_text="Special technical or other requirements for the presentation.")
    events = models.ManyToManyField(Event, related_name='articles')

    class Meta:
        app_label = 'conf_web'
        verbose_name = 'Научная статья'
        verbose_name_plural = 'Научные статьи'

    def __str__(self):
        return self.title


class ArticleSession(models.Model):
    article = models.ForeignKey(ScientificArticle, on_delete=models.CASCADE, related_name='sessions')
    time_slot = models.ForeignKey(TimeSlot, on_delete=models.CASCADE, related_name='article_sessions')
    is_confirmed = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.article.title} @ {self.time_slot}"


class ConferenceSchedule(models.Model):
    conference = models.OneToOneField(
        Conference,
        on_delete=models.CASCADE,
        related_name='schedule',
        verbose_name='Conference'
    )
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Conference Schedule'
        verbose_name_plural = 'Conference Schedules'

    def __str__(self):
        return f"Schedule for {self.conference.title}"

    # Дополнительные методы для управления расписанием
    def add_session(self, article, start_time, end_time):
        time_slot, created = TimeSlot.objects.get_or_create(
            start_time=start_time,
            end_time=end_time,
            conference=self.conference
        )
        ArticleSession.objects.create(
            article=article,
            time_slot=time_slot
        )

    def remove_session(self, article_session):
        article_session.delete()

    def update_time_slot(self, time_slot, new_start, new_end):
        time_slot.start_time = new_start
        time_slot.end_time = new_end
        time_slot.save()
